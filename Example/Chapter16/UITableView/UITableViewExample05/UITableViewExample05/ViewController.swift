//
//  ViewController.swift
//  UITableViewExample05
//
//  Created by 游鴻斌 on 2021/4/8.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    private func setupTableView() {
        let cellNib = UINib(nibName: "MyTableViewCell", bundle: .main)
        tableView.register(cellNib, forCellReuseIdentifier: "Cell")
        tableView.dataSource = self
    }

}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? MyTableViewCell else {
            fatalError("重用辨識碼錯誤")
        }
        
        cell.myImageView.image = UIImage(named: "Apple")
        return cell
    }
}

