//
//  MyTableViewController.swift
//  UITableViewExample07
//
//  Created by 游鴻斌 on 2021/4/8.
//

import UIKit

class MyTableViewController: UITableViewController {

    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var addressTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        let name = nameTextField.text ?? ""
        let phone = phoneTextField.text ?? ""
        let address = addressTextField.text ?? ""
        print("姓名: \(name)")
        print("電話: \(phone)")
        print("地址: \(address)")
    }

}
