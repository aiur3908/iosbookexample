# UITableView

### 範例01
基本使用方式

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter16/UITableView/UITableViewExample01)

### 範例02
設置Footer與Header

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter16/UITableView/UITableViewExample02)

### 範例03
使用重用機制

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter16/UITableView/UITableViewExample03)

### 範例04
客製化UITableViewCell

[範例04](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter16/UITableView/UITableViewExample04)


### 範例05
使用Xib來製作客製化UITableViewCell

[範例05](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter16/UITableView/UITableViewExample05)

### 範例06
解決重用機制的問題

[範例06](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter16/UITableView/UITableViewExample06)
