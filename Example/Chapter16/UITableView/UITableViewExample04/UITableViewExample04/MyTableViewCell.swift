//
//  MyTableViewCell.swift
//  UITableViewExample04
//
//  Created by 游鴻斌 on 2021/4/8.
//

import UIKit

class MyTableViewCell: UITableViewCell {
    
    @IBOutlet var myImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
