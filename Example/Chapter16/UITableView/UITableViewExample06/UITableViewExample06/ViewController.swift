//
//  ViewController.swift
//  UITableViewExample06
//
//  Created by 游鴻斌 on 2021/4/8.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    var tableViewData: [Bool] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    private func setupTableView() {
        for _ in 0...15 {
            tableViewData.append(true)
        }
        
        tableView.dataSource = self
    }
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? MyTableViewCell else {
            fatalError("重用辨識碼錯誤")
        }
        
        cell.mySwitch.isOn = tableViewData[indexPath.row]
        cell.delegate = self
        return cell
    }
}

extension ViewController: MyTableViewCellDelegate {
    func myTableViewCell(_ myTableViewCell: MyTableViewCell, didChangeSwithIsOn isOn: Bool) {
        if let indexPath = tableView.indexPath(for: myTableViewCell) {
            tableViewData[indexPath.row] = isOn
        }
    }
}

