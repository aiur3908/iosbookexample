//
//  MyTableViewCell.swift
//  UITableViewExample06
//
//  Created by 游鴻斌 on 2021/4/8.
//

import UIKit

protocol MyTableViewCellDelegate: AnyObject {
    func myTableViewCell(_ myTableViewCell: MyTableViewCell,
                         didChangeSwithIsOn isOn: Bool)
}


class MyTableViewCell: UITableViewCell {
    
    @IBOutlet var mySwitch: UISwitch!
    weak var delegate: MyTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func switchDidValueChanged(_ sender: UISwitch) {
        delegate?.myTableViewCell(self, didChangeSwithIsOn: sender.isOn)
    }

}
