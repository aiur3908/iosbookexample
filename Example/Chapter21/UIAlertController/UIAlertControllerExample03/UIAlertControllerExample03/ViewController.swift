//
//  ViewController.swift
//  UIAlertControllerExample03
//
//  Created by 游鴻斌 on 2021/4/8.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func buttonTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "請選擇", message: "最喜歡哪種動物?", preferredStyle: .actionSheet)
        let catAction = UIAlertAction(title: "貓", style: .default, handler: nil)
        let dogAction = UIAlertAction(title: "狗", style: .default, handler: nil)
        let dolphinAction = UIAlertAction(title: "海豚", style: .default, handler: nil)
        alertController.addAction(catAction)
        alertController.addAction(dogAction)
        alertController.addAction(dolphinAction)
        

        let cancelAction = UIAlertAction(title: "都不喜歡 :(", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
}

