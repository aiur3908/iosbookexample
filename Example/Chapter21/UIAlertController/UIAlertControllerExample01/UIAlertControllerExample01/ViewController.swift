//
//  ViewController.swift
//  UIAlertControllerExample01
//
//  Created by 游鴻斌 on 2021/4/8.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func buttonTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "請選擇", message: "你喜歡貓還是狗?", preferredStyle: .alert)

        let catAction = UIAlertAction(title: "貓", style: .default, handler: { _ in
            print("你喜歡貓!")
        })

        let dogAction = UIAlertAction(title: "狗", style: .default, handler: { _ in
            print("你喜歡狗!")
        })

        alertController.addAction(catAction)
        alertController.addAction(dogAction)
        present(alertController, animated: true, completion: nil)
    }
    
}

