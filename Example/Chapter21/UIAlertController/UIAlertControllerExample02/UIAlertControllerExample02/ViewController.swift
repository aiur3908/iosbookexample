//
//  ViewController.swift
//  UIAlertControllerExample02
//
//  Created by 游鴻斌 on 2021/4/8.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func buttonTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "提示", message: "請輸入帳號密碼", preferredStyle: .alert)
        alertController.addTextField(configurationHandler: { textField in
            textField.placeholder = "帳號"
       
        })
       
        alertController.addTextField(configurationHandler: { textField in
            textField.placeholder = "密碼"
            textField.isSecureTextEntry = true
        })
       
        let loginAction = UIAlertAction(title: " 登入 ", style: .default, handler: { _ in
            if let textFields = alertController.textFields,
               let account = textFields[0].text,
               let password = textFields[1].text {
                print("帳號:\(account), 密碼: \(password)")
            }
        })
        
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        alertController.addAction(loginAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
}

