# UIAlertController

### 範例01
基本語法

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter21/UIAlertController/UIAlertControllerExample01)

### 範例02
增加輸入框到UIAlertController之中

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter21/UIAlertController/UIAlertControllerExample02)

### 範例03
動作表

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter21/UIAlertController/UIAlertControllerExample03)


