//
//  ViewController.swift
//  UIScrollViewExample03
//
//  Created by 游鴻斌 on 2021/4/6.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 5
        scrollView.delegate = self
        setupGestureRecognizer()
    }
    
    private func setupGestureRecognizer() {
        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        // 需要點兩下才會觸發
        doubleTapRecognizer.numberOfTapsRequired = 2
        // 於 UIScrollView 中增加手勢辨識
        scrollView.addGestureRecognizer(doubleTapRecognizer)
    }
    
    
    @objc func handleTap() {
        if scrollView.zoomScale > scrollView.minimumZoomScale {
            // 放到最小
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        } else {
            // 放到最大
            scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
        }
    }

}

extension ViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}

