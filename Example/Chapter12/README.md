# Chapter12

##  UIScrollView

### 範例01
垂直捲動的UIScrollView

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter12/UIScrollView/UIScrollViewExample01)


### 範例02
水平捲動的UIScrollView

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter12/UIScrollView/UIScrollViewExample02)

### 範例03
縮放的UIScrollView

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter12/UIScrollView/UIScrollViewExample03)
