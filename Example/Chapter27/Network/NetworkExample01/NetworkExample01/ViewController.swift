//
//  ViewController.swift
//  NetworkExample01
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var imageView: UIImageView!

    @IBAction func buttonTapped(_ sender: Any) {
        if let url = URL(string: "https://i.imgur.com/ILR302C.jpg") {
            let urlRequest = URLRequest(url: url)
            let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                if let data = data {
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        self.imageView.image = image
                    }
                }
            }
            task.resume()
        }
    }
    
}
