# Chapter11

###  範例01 
垂直水平置中於畫面中央，且寬高均為 150

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter11/Example1101)

###  範例02 
寬高利用距離來定義

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter11/Example1102)

### 範例03
寬高使用比例來設定

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter11/Example1103)

### 範例04
與其他 View 做比較，定義寬高

[範例04](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter11/Example1104)

### 範例05
與其他 View 做比較，定義位置

[範例05](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter11/Example1105)

### 範例06
與其他 View 做比較，定義垂直或水平置中

[範例06](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter11/Example1106)

### 範例07
設置優先度

[範例07](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter11/Example1107)
