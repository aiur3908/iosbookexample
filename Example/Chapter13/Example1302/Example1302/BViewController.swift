//
//  BViewController.swift
//  Example1302
//
//  Created by 游鴻斌 on 2021/4/6.
//

import UIKit

class BViewController: UIViewController {

    @IBOutlet var nameLabel: UILabel!
    var name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = name
    }    

}
