//
//  ViewController.swift
//  Example1302
//
//  Created by 游鴻斌 on 2021/4/6.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonTapped(_ sender: Any) {
        performSegue(withIdentifier: "DisplayBVC", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let bVC = segue.destination as? BViewController {
            bVC.name = nameTextField.text ?? ""
        }
    }
    
}

