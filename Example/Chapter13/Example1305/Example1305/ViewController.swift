//
//  ViewController.swift
//  Example1305
//
//  Created by 游鴻斌 on 2021/4/6.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var nameLabel: UILabel!
    var name  = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    @IBAction func backToViewController(segue: UIStoryboardSegue) {
        nameLabel.text = name
    }

}

