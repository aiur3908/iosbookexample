//
//  BViewController.swift
//  Example1305
//
//  Created by 游鴻斌 on 2021/4/6.
//

import UIKit

class BViewController: UIViewController {
    
    @IBOutlet var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let viewController = segue.destination as? ViewController {
            viewController.name = nameTextField.text ?? ""
        }
    }

}
