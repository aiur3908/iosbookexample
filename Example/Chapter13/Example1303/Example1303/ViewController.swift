//
//  ViewController.swift
//  Example1303
//
//  Created by 游鴻斌 on 2021/4/6.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func buttonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        let bVC = storyboard.instantiateViewController(withIdentifier: "BViewController")
        present(bVC, animated: true, completion: nil)
    }
    
}

