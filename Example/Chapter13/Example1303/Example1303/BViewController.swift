//
//  BViewController.swift
//  Example1303
//
//  Created by 游鴻斌 on 2021/4/6.
//

import UIKit

class BViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
