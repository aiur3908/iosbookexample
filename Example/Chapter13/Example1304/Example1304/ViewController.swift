//
//  ViewController.swift
//  Example1304
//
//  Created by 游鴻斌 on 2021/4/6.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func presentButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let cVC = storyboard.instantiateViewController(withIdentifier: "CViewController")
        present(cVC, animated: true, completion: nil)
    }
    
}

