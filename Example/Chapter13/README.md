# Chapter13

### 範例01
透過Storyboard切換頁面

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter13/Example1301)

### 範例02
透過Segue切換頁面，並且傳遞資料

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter13/Example1302)

### 範例03
透過Storyboard產生UIViewController，並透過present切換頁面

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter13/Example1303)

### 範例04
不同Storyboard之間切換頁面

[範例04](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter13/Example1304)


### 範例05
透過Unwind Segue將資料傳遞到第一個頁面

[範例05](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter13/Example1305)

### 範例06
透過Unwind Segue一口氣回到第一頁

[範例06](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter13/Example1306)
