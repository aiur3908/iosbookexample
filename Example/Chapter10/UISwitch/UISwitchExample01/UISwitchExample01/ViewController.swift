//
//  ViewController.swift
//  UISwitchExample01
//
//  Created by 游鴻斌 on 2021/3/23.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func switchDidValueChanged(_ sender: UISwitch) {
        print(sender.isOn)
    }
    
}

