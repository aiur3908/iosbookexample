//
//  ViewController.swift
//  ClosureExample01
//
//  Created by 游鴻斌 on 2021/4/8.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    var tableViewData: [Bool] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    private func setupTableView() {
        for _ in 0...15 {
            tableViewData.append(true)
        }
        
        let cellNib = UINib(nibName: "MyTableViewCell", bundle: .main)
        tableView.register(cellNib, forCellReuseIdentifier: "Cell")
        tableView.dataSource = self
    }

}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? MyTableViewCell else {
            fatalError()
        }

        cell.mySwitch.isOn = tableViewData[indexPath.row]
        cell.switchChangedHandler = { [unowned self] isOn in
            self.tableViewData[indexPath.row] = isOn
        }
        return cell
    }
}
