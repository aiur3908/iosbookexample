//
//  MyTableViewCell.swift
//  ClosureExample01
//
//  Created by 游鴻斌 on 2021/4/8.
//

import UIKit

class MyTableViewCell: UITableViewCell {

    @IBOutlet var mySwitch: UISwitch!
    var switchChangedHandler: ((Bool) -> Void)?

    @IBAction func switchDidValueChanged(_ sender: UISwitch) {
        switchChangedHandler?(sender.isOn)
    }
    
}
