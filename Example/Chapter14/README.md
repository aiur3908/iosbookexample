# Chapter 14

## UIPickerView

### 範例01
基本使用方式

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter14/UIPickerView/UIPickerViewExample01)

### 範例02
透過陣列來設置

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter14/UIPickerView/UIPickerViewExample02)

### 範例03
建立多個Component的UIPickerView

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter14/UIPickerView/UIPickerViewExample03)
