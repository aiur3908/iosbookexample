//
//  ViewController.swift
//  UINavigationControllerExample04
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class ViewController: UIViewController {
    
    var tableViewData: [String] = []
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    private func setupTableView() {
        for index in 0...10 {
            tableViewData.append(String(index))
        }
        tableView.dataSource = self
        tableView.delegate = self
    }

    @IBAction func editButtonTapped(_ sender: Any) {
        tableView.setEditing(true, animated: true)
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        tableView.setEditing(false, animated: true)
    }
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") else {
            fatalError()
        }
        
        cell.textLabel?.text = tableViewData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //移除資料
            tableViewData.remove(at: indexPath.row)
            //移除Row
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}


extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
}

