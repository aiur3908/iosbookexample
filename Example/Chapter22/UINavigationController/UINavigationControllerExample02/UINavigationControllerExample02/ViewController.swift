//
//  ViewController.swift
//  UINavigationControllerExample02
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func leftButtonTapped(_ sender: Any) {
        print("左邊按鈕點擊")
    }
    
    @IBAction func rightButtonTapped(_ sender: Any) {
        print("右邊按鈕點擊")
    }
    
    
}

