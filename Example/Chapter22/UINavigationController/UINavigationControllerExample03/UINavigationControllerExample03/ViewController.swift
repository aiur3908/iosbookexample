//
//  ViewController.swift
//  UINavigationControllerExample03
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    var tableViewData: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    private func setupTableView() {
        for index in 0...10 {
            tableViewData.append(String(index))
        }
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @IBAction func addButtonTapped(_ sender: Any) {
        tableView.setEditing(true, animated: true)
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        tableView.setEditing(false, animated: true)
    }
    
    
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        // 取出原本位置的資料
        let sourceData = tableViewData[sourceIndexPath.row]
        // 將原本位置的資料移除
        tableViewData.remove(at: sourceIndexPath.row)
        // 新增到新的位置
        tableViewData.insert(sourceData, at: destinationIndexPath.row)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") else {
            fatalError()
        }
        
        cell.textLabel?.text = tableViewData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .insert {
            tableViewData.insert(String(Int.random(in: 0...10)),
                                    at: indexPath.row + 1)
            let newIndexPath = IndexPath(row: indexPath.row + 1,
                                         section: indexPath.section)
            tableView.beginUpdates()
            tableView.insertRows(at: [newIndexPath], with: .fade)
            tableView.endUpdates()
        }
    }
}


extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .insert
    }
}
