# UINavigationController

### 範例01
基本使用方式

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter22/UINavigationController/UINavigationControllerExample01)

### 範例02
增加UIBarButtonItem

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter22/UINavigationController/UINavigationControllerExample02)

### 範例03
UINavigationController + UITableView，擁有增加與移動的功能

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter22/UINavigationController/UINavigationControllerExample03)

### 範例04
UINavigationController + UITableView，擁有刪除的功能

[範例04](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter22/UINavigationController/UINavigationControllerExample04)
