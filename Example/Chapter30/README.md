# Chapter 30

## Timer

### 範例01
基本使用方式

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter30/Timer/TimerExample01)

### 範例02
倒數計時範例

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter30/Timer/TimerExample02)
