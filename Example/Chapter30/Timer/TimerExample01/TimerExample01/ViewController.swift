//
//  ViewController.swift
//  TimerExample01
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class ViewController: UIViewController {

    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = Timer.scheduledTimer(timeInterval: 1,
                                     target: self,
                                     selector: #selector(sayHello(timer:)),
                                     userInfo: nil,
                                     repeats: true)
    }

    @objc func sayHello(timer: Timer) {
        print("Hello")
    }

}

