//
//  ViewController.swift
//  TimerExample02
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var infoLabel: UILabel!
    
    var seconds = 10
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        timer = Timer.scheduledTimer(
            timeInterval: 1,
            target: self,
            selector: #selector(countdown(timer:)),
            userInfo: nil,
            repeats: true)
    }
    
    @objc func countdown(timer: Timer) {
        seconds -= 1
        if seconds == 0 {
            // 時間到
            infoLabel.text = "新年快樂"
            timer.invalidate()
        } else {
            infoLabel.text = "\(seconds)"
        }
    }
}

