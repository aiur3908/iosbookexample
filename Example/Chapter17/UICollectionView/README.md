# UICollectionView

### 範例01
基本使用方式

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter17/UICollectionView/UICollectionViewExample01)

### 範例02
透過XIB來客製化UICollectionViewCell

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter17/UICollectionView/UICollectionViewExample02)

### 範例03
使用UICollectionViewFlowLayout

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter17/UICollectionView/UICollectionViewExample03)

### 範例04
設置Footer與Header

[範例04](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter17/UICollectionView/UICollectionViewExample04)
