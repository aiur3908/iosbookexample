//
//  ViewController.swift
//  BundleExample01
//
//  Created by 游鴻斌 on 2021/4/11.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        loadFileContent()
    }
    
    private func loadFileContent() {
        do {
            if let path = Bundle.main.path(forResource: "出師表", ofType: "txt") {
                let content = try String(contentsOfFile: path)
                print(content)
            }
        } catch {
            print(error.localizedDescription)
        }
    }

}

