//
//  ViewController.swift
//  CoreDataExample01
//
//  Created by 游鴻斌 on 2021/4/11.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let persistentContainer = appDelegate.persistentContainer
            let context = persistentContainer.viewContext
            if let student = NSEntityDescription.insertNewObject(forEntityName: "Student",
                                                                 into: context) as? Student {
                student.name = "Jerry"
                student.age = 30
                student.id = "110123001"
                do {
                    try context.save()
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
    }
}

