//
//  ViewController.swift
//  UIPinchGestureRecognizerExample01
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class ViewController: UIViewController {

    var myView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let rect = CGRect(x: 100,
                              y: 100,
                          width: 100,
                          height: 100)
        myView = UIView(frame: rect)
        myView.backgroundColor = UIColor.red
        view.addSubview(myView)
        let recognizer = UIPinchGestureRecognizer(target: self,
                                                  action: #selector(handlePinch(recognizer:)))
        view.addGestureRecognizer(recognizer)
    }
    
    
    @objc func handlePinch(recognizer: UIPinchGestureRecognizer) {
        let scale = recognizer.scale
        myView.transform = myView.transform.scaledBy(x: scale, y: scale)
        recognizer.scale = 1
    }

}

