//
//  ViewController.swift
//  UITapGestureRecognizerExample01
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(recognizer)
    }
    
    @objc func handleTap() {
        print("使用者點擊")
    }

}

