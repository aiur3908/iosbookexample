//
//  ViewController.swift
//  UIRotationGestureRecognizerExample01
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class ViewController: UIViewController {

    var myView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let rect = CGRect(x: 100,
                          y: 100,
                          width: 100,
                          height: 100)

        myView = UIView(frame:rect)
        myView.backgroundColor = UIColor.red
        view.addSubview(myView)

        let rotation = UIRotationGestureRecognizer(target: self,
                                                   action: #selector(rotation(recognizer:)))
        view.addGestureRecognizer(rotation)
    }
    
    @objc func rotation(recognizer: UIRotationGestureRecognizer) {
        // 弧度
        let radian = recognizer.rotation
        myView.transform = myView.transform.rotated(by: radian)
        // 重置
        recognizer.rotation = 0
    }
}

