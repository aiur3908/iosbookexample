# Chapter 24

## UITapGestureRecognizer

### 範例01
點擊手勢辨識器

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter24/UITapGestureRecognizerExample01)

## UILongPressGestureRecognizer

### 範例02
長按手勢識別器

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter24/UILongPressGestureRecognizerExample01)

## UIPanGestureRecognizer

### 範例03
拖曳手勢識別器

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter24/UIPanGestureRecognizerExample01)

## UIPinchGestureRecognizer

### 範例04
縮放手勢辨識器

[範例04](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter24/UIPinchGestureRecognizerExample04)

## UIRotationGestureRecognizer
旋轉手勢辨識器

[範例05](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter24/UIRotationGestureRecognizerExample01)
