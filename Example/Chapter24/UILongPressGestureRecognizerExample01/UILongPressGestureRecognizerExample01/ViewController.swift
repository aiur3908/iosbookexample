//
//  ViewController.swift
//  UILongPressGestureRecognizerExample01
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let recognizer = UILongPressGestureRecognizer(target: self,
                                                      action: #selector(handleLongPress(recognizer:)))
        view.addGestureRecognizer(recognizer)
    }
    
    @objc func handleLongPress(recognizer: UILongPressGestureRecognizer) {
        if recognizer.state == .began {
            print("長按開始")
        } else if recognizer.state == .changed {
            print("長按變化")
        } else if recognizer.state == .ended {
            print("長按結束")
        }
    }

}

