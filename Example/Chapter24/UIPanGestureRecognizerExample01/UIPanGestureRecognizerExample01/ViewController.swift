//
//  ViewController.swift
//  UIPanGestureRecognizerExample01
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class ViewController: UIViewController {

    var myView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let rect = CGRect(x: 100,
                          y: 100,
                          width: 100,
                          height: 100)
        myView = UIView(frame: rect)
        myView.backgroundColor = UIColor.red
        view.addSubview(myView)
        let recognizer = UIPanGestureRecognizer(target: self, action: #selector(move(recognizer:)))
        myView.addGestureRecognizer(recognizer)
    }
    
    
    @objc func move(recognizer: UIPanGestureRecognizer) {
        let point = recognizer.location(in: view)
        // 更改 myView 的位置
        myView.center = point
    }

}

