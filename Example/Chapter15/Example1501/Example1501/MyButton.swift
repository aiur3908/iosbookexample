//
//  MyButton.swift
//  Example1501
//
//  Created by 游鴻斌 on 2021/4/7.
//

import UIKit

protocol MyButtonDataSource: AnyObject {
    func background(in myButton: MyButton) -> UIColor
}

@objc protocol MyButtonDelegate: AnyObject {
    @objc func myButtonDidTapped(_ myButton: MyButton)
}

class MyButton: UIButton {

    weak var dataSource: MyButtonDataSource?
    weak var delegate: MyButtonDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAction()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupAction()
    }
    
    private func setupAction() {
        addTarget(self, action: #selector(tapped), for: .touchUpInside)
    }
    
    @objc private func tapped() {
        delegate?.myButtonDidTapped(self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = dataSource?.background(in: self)
    }

}
