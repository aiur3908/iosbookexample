//
//  ViewController.swift
//  Example1501
//
//  Created by 游鴻斌 on 2021/4/7.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let myButton = MyButton(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
        myButton.dataSource = self
        myButton.delegate = self
        view.addSubview(myButton)
    }
}

extension ViewController: MyButtonDataSource {
    func background(in myButton: MyButton) -> UIColor {
        return .red
    }
}

extension ViewController: MyButtonDelegate {
    func myButtonDidTapped(_ myButton: MyButton) {
        print("MyButton 被點擊囉 ")
    }
}
