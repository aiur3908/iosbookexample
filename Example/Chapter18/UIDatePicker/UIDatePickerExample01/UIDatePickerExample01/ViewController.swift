//
//  ViewController.swift
//  UIDatePickerExample01
//
//  Created by 游鴻斌 on 2021/4/8.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDatePicker()
    }
    
    private func setupDatePicker() {
        // 最小日期為今天
        let minDate = Date()
        datePicker.minimumDate = minDate
        // 最大日期為今天加上一年
        let maxDate = Calendar.current.date(byAdding: .year, value: 1, to: Date())
        datePicker.maximumDate = maxDate
    }

    @IBAction func buttonTapped(_ sender: Any) {
        let date = datePicker.date
        print(date)
    }
    
}

