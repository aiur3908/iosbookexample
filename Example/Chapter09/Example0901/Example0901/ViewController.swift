//
//  ViewController.swift
//  Example0901
//
//  Created by 游鴻斌 on 2021/3/23.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var myLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myLabel.text = "Hello World"
    }

}

