# 客製化View
### 範例01
基本範例

[範例01](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter23/CustomView/CustomViewExample01)

### 範例02
增加UI元件

[範例02](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter23/CustomView/CustomViewExample02)

### 範例03
利用XIB來製作客製化View

[範例03](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter23/CustomView/CustomViewExample03)

### 範例04
客製化UIControl

[範例04](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter23/CustomView/CustomViewExample04)

### 範例05
IBDesignable 與 IBInspectable

[範例05](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter23/CustomView/CustomViewExample05)


### 範例06
客製化UIView + DataSource 與 Delegate

[範例06](https://gitlab.com/aiur3908/iosbookexample/-/tree/master/Example/Chapter23/CustomView/CustomViewExample06)
