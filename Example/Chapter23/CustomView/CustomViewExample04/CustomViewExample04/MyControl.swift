//
//  MyControl.swift
//  CustomViewExample04
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class MyControl: UIControl {

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.blue
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        backgroundColor = UIColor.blue
    }
}
