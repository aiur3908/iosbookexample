//
//  ViewController.swift
//  CustomViewExample06
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var myView: MyView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myView.dataSource = self
        myView.delegate = self
    }

}

extension ViewController: MyViewDataSource {
    func colorForLeftView(in myView: MyView) -> UIColor {
        return .red
    }
    
    func colorForRightView(in myView: MyView) -> UIColor {
        return .green
    }
}

extension ViewController: MyViewDelegate {
    func didTappedLeftView(in myView: MyView) {
        print("左邊的View被點擊")
    }
    
    func didTappedRightView(in myView: MyView) {
        print("右邊的View被點擊")
    }
}

