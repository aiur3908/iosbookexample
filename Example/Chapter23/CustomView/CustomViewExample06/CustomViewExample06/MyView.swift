//
//  MyView.swift
//  CustomViewExample06
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

protocol MyViewDataSource: AnyObject {
    func colorForLeftView(in myView: MyView) -> UIColor
    func colorForRightView(in myView: MyView) -> UIColor
}

@objc protocol MyViewDelegate: AnyObject {
    @objc optional func didTappedLeftView(in myView: MyView)
    @objc optional func didTappedRightView(in myView: MyView)
}

class MyView: UIView {
    @IBOutlet var leftView: UIView!
    @IBOutlet var rightView: UIView!
    
    weak var delegate: MyViewDelegate?
    weak var dataSource: MyViewDataSource?

    override init(frame: CGRect) {
        super.init(frame: frame)
        loadXibView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadXibView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        leftView.backgroundColor = dataSource?.colorForLeftView(in: self)
        rightView.backgroundColor = dataSource?.colorForRightView(in: self)
    }
    
    private func loadXibView() {
        // 取得當前的 Bundle
        let bundle = Bundle(for: type(of: self))
        // 透過當前的 Bundle 與 xib 名稱取得 xib 實體
        let nib = UINib(nibName: "MyView", bundle: bundle)
        // 取得 xib 內的第一個畫面
        guard let xibView = nib.instantiate(withOwner: self,
                                            options: nil).first as? UIView else {
            return
        }
        addSubview(xibView)
        xibView.translatesAutoresizingMaskIntoConstraints = false
        xibView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        xibView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        xibView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        xibView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    
    @IBAction func leftButtonTapped(_ sender: Any) {
        delegate?.didTappedLeftView?(in: self)
    }
    
    @IBAction func rightButtonTapped(_ sender: Any) {
        delegate?.didTappedRightView?(in: self)
    }
    
}
