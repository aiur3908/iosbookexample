//
//  ViewController.swift
//  CustomViewExample01
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let rect = CGRect(x: 100,
                          y: 100,
                          width: 100,
                          height: 100)
        let myView = MyView(frame: rect)
        view.addSubview(myView)
    }

}

