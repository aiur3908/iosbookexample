//
//  MyView.swift
//  CustomViewExample01
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class MyView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .red
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        backgroundColor = .red
    }

}
