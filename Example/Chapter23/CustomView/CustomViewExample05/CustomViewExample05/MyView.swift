//
//  MyView.swift
//  CustomViewExample05
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

@IBDesignable
class MyView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

}
