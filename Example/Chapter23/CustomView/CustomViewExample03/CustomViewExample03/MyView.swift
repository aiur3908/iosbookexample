//
//  MyView.swift
//  CustomViewExample03
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class MyView: UIView {

    @IBOutlet var label: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        loadXibView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadXibView()
    }
    
    private func loadXibView() {
        // 取得當前的 Bundle
        let bundle = Bundle(for: type(of: self))
        // 透過當前的 Bundle 與 xib 名稱取得 xib 實體
        let nib = UINib(nibName: "MyView", bundle: bundle)
        // 取得 xib 內的第一個畫面
        guard let xibView = nib.instantiate(withOwner: self,
                                            options: nil).first as? UIView else {
            return
        }
        addSubview(xibView)
        xibView.translatesAutoresizingMaskIntoConstraints = false
        xibView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        xibView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        xibView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        xibView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
}
