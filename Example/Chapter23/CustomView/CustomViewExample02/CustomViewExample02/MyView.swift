//
//  MyView.swift
//  CustomViewExample02
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class MyView: UIView {

    var label: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLabel()
        backgroundColor = .red
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupLabel()
        backgroundColor = .red
    }
    
    private func setupLabel() {
        label = UILabel(frame: .zero)
        addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }

}
