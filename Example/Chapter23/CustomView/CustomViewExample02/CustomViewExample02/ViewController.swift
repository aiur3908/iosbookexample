//
//  ViewController.swift
//  CustomViewExample02
//
//  Created by 游鴻斌 on 2021/4/10.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var myView: MyView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myView.label.text = "Hello World!"
    }


}

