//
//  ViewController.swift
//  Homework0903
//
//  Created by 游鴻斌 on 2021/5/31.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var myLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myLabel.text = "Hello, Swift"
    }


}

