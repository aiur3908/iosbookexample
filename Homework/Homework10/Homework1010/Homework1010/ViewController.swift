//
//  ViewController.swift
//  Homework1010
//
//  Created by 游鴻斌 on 2021/5/31.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var myLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func silderValueChanged(_ sender: UISlider) {
        myLabel.text = String(sender.value)
    }
    
}

