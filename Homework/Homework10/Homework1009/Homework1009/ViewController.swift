//
//  ViewController.swift
//  Homework1009
//
//  Created by 游鴻斌 on 2021/5/31.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var myView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func segmentControlVauleChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            myView.backgroundColor = UIColor.red
        } else if sender.selectedSegmentIndex == 1 {
            myView.backgroundColor = UIColor.green
        } else if sender.selectedSegmentIndex == 2 {
            myView.backgroundColor = UIColor.blue
        }
    }
    
}

