//
//  ViewController.swift
//  Homework1007
//
//  Created by 游鴻斌 on 2021/5/31.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var accountTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func loginButtonTapped(_ sender: Any) {
        print("帳號 \(accountTextField.text ?? "")")
        print("密碼 \(passwordTextField.text ?? "")")
    }
    
}

