//
//  ViewController.swift
//  Homework1006
//
//  Created by 游鴻斌 on 2021/5/31.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var myView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func switchValueChanged(_ sender: UISwitch) {
        myView.isHidden = sender.isOn
    }
    
}

